mavennatives
========================

How can you include natives in the java proejct?

Simple! Use maven-nativedependencies-plugin.

  * http://code.google.com/p/mavennatives/

# How to use

Next step, how to lookup and find those native librarys?

## JNA

Use this sample:

```java
import java.io.File;

import com.github.axet.mavennatives.MavenNatives;
import com.sun.jna.NativeLibrary;
import org.bridj.BridJ;

public class YourNativeWrapper {

    static {
        // keep librarys in order. example for jna library
        //
        // <groupId>net.java.dev.jna</groupId>
        // <artifactId>platform</artifactId>
        // <version>3.5.1</version>
        File file = MavenNatives.mavenNative("LIBRARY1");
        
        NativeLibrary.addSearchPath("LIBRARY1", file.getParent());
        NativeLibrary.getInstance("LIBRARY1");

        File file2 = MavenNatives.mavenNatives("LIBRARY2_DEPEND_ON_LIBRARY1");
        
        NativeLibrary.addSearchPath("LIBRARY2_DEPEND_ON_LIBRARY1", file2.getParent());
        NativeLibrary.getInstance("LIBRARY2_DEPEND_ON_LIBRARY1");
    }
}
```

## BridJ

Use this sample:

```java
import java.io.File;

import com.github.axet.mavennatives.MavenNatives;
import com.sun.jna.NativeLibrary;
import org.bridj.BridJ;

public class YourNativeWrapper {

    static {
        // keep librarys in order. example for jna library
        //
        // <groupId>net.java.dev.jna</groupId>
        // <artifactId>platform</artifactId>
        // <version>3.5.1</version>

        // JNAerator 0.11
        //
        // <groupId>com.nativelibs4java</groupId>
        // <artifactId>bridj</artifactId>
        // <version>0.6.2</version>
        File file3 = MavenNatives.mavenNative("torrent");
        BridJ.setNativeLibraryFile("torrent", file3);
    }
}
```

# Maven Central Repo

```xml
<dependency>
  <groupId>com.github.axet</groupId>
  <artifactId>mavennatives</artifactId>
  <version>0.0.9</version>
</dependency>
```
